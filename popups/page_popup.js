document.addEventListener("click", function(e) {
   if (!e.target.classList.contains("item")) {
       return;
   }
   var id = e.target.id;

   if (id === "prefs") {
       browser.runtime.openOptionsPage();
   } else if (id === "reload") {
    browser.runtime.sendMessage({msg: "reload"});
   }
   window.close();
});
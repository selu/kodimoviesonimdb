
var rcode = /tt\d+(?=\/?(?:\?.*)?$)/;

function handleResponse(element, response) {
  if (response.have) {
    element.classList.add('mymovie');
    if (response.played) {
      element.classList.add('played');
    }
  } else {
    element.classList.remove('mymovie', 'played');
  }
}

function refreshHighlights() {
  document.body.querySelectorAll("a[href^='/title/tt']").forEach( (element) => {
    var moviecode = element.href.match(rcode);
    if (moviecode) {
      browser.runtime.sendMessage({msg: "movie", code: moviecode[0]})
        .then(handleResponse.bind(null, element));
    }
  });
}

browser.runtime.onMessage.addListener(request => {
  if (request.message == 'refresh') {
    refreshHighlights();
  }
});

refreshHighlights();
